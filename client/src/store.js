import {
  applyMiddleware,
  combineReducers,
  compose,
  createStore
} from 'redux'

const layouts = () => [
  {
    name: 'I have a scorecard',
    id: 'NoComponentLayouts1234123',
    components: [
      {
        name: 'ScoreCard',
        id: 'ScoreCard',
        component: 'ScoreCard',
        enabled: true,
        state: {}
      },
    ]
  },
  {
    name: 'I have a logo',
    id: 'BaconLayout1234567',
    components: [
      {
        name: 'Logo',
        id: 'Logo12341313',
        component: 'Logo',
        enabled: true,
        settings: {},
        state: {
          url: 'https://www.petmd.com/sites/default/files/petmd-kitten-development.jpg'
        }
      },
    ],
  }
]

let initialLayouts = []

if(localStorage && localStorage.getItem('hlstore')) {
  initialLayouts = () => JSON.parse(localStorage.getItem('hlstore')).layouts
} else {
  initialLayouts = layouts
}

const rootReducer = combineReducers({
    layouts: initialLayouts
})

function wrapAction(action) {
  return {
    action,
    time: Date.now(),
  }
}


function storageMiddleware() {
  return () => next => action => {
    const wrappedAction = wrapAction(action)

    localStorage.setItem(
      'hlstore',
      JSON.stringify(wrappedAction)
    )

    next(action)
  }
}

export function createStorageListener(store) {
  return event => {
    const { action } = JSON.parse(event.newValue)
    store.dispatch(action)
  }
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(storageMiddleware())),
)

localStorage.setItem(
  'hlstore',
  JSON.stringify(store.getState()),
)

// need this for stream panes
// window.addEventListener('storage', createStorageListener(store))


export default store
