import React, { Component } from 'react'
import { string } from 'prop-types'

export default class Controls extends Component {
  static propTypes = {
    url: string,
  }

  static defaultProps = {
    url: 'https://cdn-images-1.medium.com/max/1600/1*b8Fkajj_Wxn0j32ia3PNfQ.png',
  }

  onClick = () => {
    const { layout, key } = this.props

  }

  render() {
    console.log(this.props.state)
    console.log(this.props)
    const params = { ...this.props, ...this.props.state }
    const { url } = params

    return (
      <div>
        <p>Stream Controls</p>
        <input value={url} />
        <img src={url} />
      </div>
    )
  }
}
