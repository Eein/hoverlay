import React from 'react'
import { any } from 'prop-types'
import { NavLink as ReactRouterLink } from 'react-router-dom'

export default function Link({ children, ...rest }) {
  return (
    <ReactRouterLink exact activeClassName='is-active' {...rest}>{children}</ReactRouterLink>
  )
}

Link.propTypes = {
  children: any,
}
