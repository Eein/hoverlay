import React from 'react'
import Link from 'shared/components/Link'

export default function Menu() {
  return (
    <aside className='menu'>
      <p className='menu-label'>
        General
      </p>
      <ul className='menu-list'>
        <li><Link to='/'>Dashboard</Link></li>
        <li><Link to='/layouts'>Layouts</Link></li>
      </ul>
    </aside>
  )
}
