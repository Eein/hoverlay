import React, { PureComponent } from 'react'
import { object, string, bool } from 'prop-types'
import styles from './Card.module.scss'
import * as widgets from 'widgets'

export default class Card extends PureComponent {
  static propTypes = {
    title: string,
    config: object,
    enabled: bool,
    id: string,
  }

  defaultConfig() {
    return {
      title: 'No Title',
      enabled: false,
    }
  }

  render() {
    const config = {
      ...this.defaultConfig(),
      ...this.props.config,
      ...this.props,
    }

    const { component, enabled, layout, state, settings } = this.props
    const hasSettings = settings && settings !== {}
    const id = this.props.id || Math.random().toString()
    const Controls = widgets[component][`${component}Controls`]

    return (
      <div className={styles.cardContainer}>
        <div className='card'>
          <header className='card-header'>
            <p className='card-header-title'>
              {config.title}
            </p>
            <div className='card-header-icon'>
              { enabled && <div>
                <input id={id} type='checkbox' name={id} className='switch' defaultChecked='checked' />
                <label htmlFor={id}>&nbsp;</label>
              </div> }
              { hasSettings && <div>
                <span role='img' aria-label='config'>
                  ⚙️
                </span>
              </div> }
            </div>
          </header>
          <div className='card-content'>
            <div className='content'>
              Render Component Children here
              <br/>
              I'm being used by layout: {layout.name}
              <Controls state={state} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
