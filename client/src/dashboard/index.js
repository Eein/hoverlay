import React from 'react'
import { Route } from 'react-router-dom'
import Dashboard from './containers/dashboard'
import Layout from './containers/layout'

export default function DashboardContainer() {
  return (
    <div>
      <Layout>
        <Route exact path='/' component={Dashboard} />
      </Layout>
    </div>
  )
}
