import React, { Component } from 'react'
import styles from './Dashboard.module.scss'
import Card from 'dashboard/components/Card'
import { connect } from 'react-redux'
import { array } from 'prop-types'

function mapStateToProps(state) {
  const layouts = state.layouts
  return { layouts }
}

function mapDispatchToProps(dispatch) {
  return {}
}

class Dashboard extends Component {
  static propTypes = {
    layouts: array,
  }

  renderLayouts = () => {
    const layoutsWithComponents = this.props.layouts.filter(l => { return l.components.length > 0 })
    return layoutsWithComponents.map(l => {
      return [
        <div className='menu-label' key={l.id}>{l.name}</div>,
        l.components.map(c => {
          const Component = `${c.name}`
          return <Card
            layout={l}
            key={c.id}
            title={c.name}
            component={Component}
            enabled={c.enabled}
            config={c.config}
            settings={c.settings}
            state={c.state}
          />
        })
      ]
    })
  }

  render() {
    return (
      <div className={styles.dashboardContainer}>
        <div className={styles.dashboardContent}>
          {this.renderLayouts()}
        </div>
        <div className={styles.dashboardPreview}>
          Preview
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
