import React from 'react'
import { any } from 'prop-types'
import Navbar from '../../components/Navbar'
import Menu from '../../components/Menu'
import styles from './Layout.module.scss'

export default function Layout({ children }) {
  return (
    <div>
      <div>
        <Navbar />
      </div>
      <div className={styles.bodyContainer}>
        <div className={styles.sidebarContainer}>
          <Menu />
        </div>
        <div className={styles.pageContainer}>
          {children}
        </div>
      </div>
    </div>
  )
}

Layout.propTypes = {
  children: any,
}
